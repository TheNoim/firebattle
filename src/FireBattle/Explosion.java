package FireBattle;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Color;
import org.bukkit.Effect;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Builder;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FallingBlock;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.util.Vector;

public class Explosion implements Listener{

	static ArrayList<UUID> uuid = new ArrayList();
    

    
	
	@EventHandler
	public void onEx(EntityExplodeEvent e){
		
		Entity entity = e.getEntity();
		
		if (entity.getType() == EntityType.FIREBALL || entity.getType() == EntityType.MINECART_TNT || entity.getType() == EntityType.PRIMED_TNT){
			
			//FireworkEffect.Builder builder = FireworkEffect.builder();
			//FireworkEffect effect = builder.flicker(false).trail(false).with(FireworkEffect.Type.BALL).withColor(Color.RED).withFade(Color.BLUE).build();
			//CustomEntityFirework.spawn(e.getLocation(), effect);
			FireworkEffectPlayer fplayer = new FireworkEffectPlayer();
			try {
				fplayer.playFirework(entity.getWorld(), entity.getLocation(), FireworkEffect.builder().flicker(false).trail(false).with(FireworkEffect.Type.BALL).withColor(Color.RED).withFade(Color.BLUE).build());
			} catch (IllegalArgumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ArrayList<Location> locations = new ArrayList(); 
			ArrayList<Block> blocks = (ArrayList)e.blockList();
			ArrayList<Material> materials = new ArrayList();
			ArrayList<Byte> datas = new ArrayList();
			
			e.setCancelled(true);
		    for (int i = 0; i < blocks.size(); i++)
		    {
		      materials.add(((Block)blocks.get(i)).getType());
		      locations.add(((Block)blocks.get(i)).getLocation());
		      datas.add(Byte.valueOf(((Block)blocks.get(i)).getData()));
		      e.setCancelled(true);
		    }
		    entity.getWorld().createExplosion(entity.getLocation().getX(), entity.getLocation().getY(), entity.getLocation().getZ(), 6.0F, false, false);
		    entity.getWorld().createExplosion(entity.getLocation().getY(), entity.getLocation().getY(), entity.getLocation().getZ(), 6.0F, false, false);
		    entity.getWorld().createExplosion(entity.getLocation().getZ(), entity.getLocation().getY(), entity.getLocation().getZ(), 6.0F, false, false);
		    entity.getWorld().createExplosion(entity.getLocation().getX(), entity.getLocation().getY(), entity.getLocation().getZ(), 6.0F, false, false);
		    entity.getWorld().createExplosion(entity.getLocation().getX(), entity.getLocation().getY(), entity.getLocation().getZ(), 6.0F, false, false);
		    entity.getWorld().createExplosion(entity.getLocation().getX(), entity.getLocation().getY(), entity.getLocation().getZ(), 6.0F, false, false);
		    entity.getWorld().createExplosion(entity.getLocation().getX(), entity.getLocation().getY(), entity.getLocation().getZ(), 6.0F, false, false);
		    entity.getWorld().createExplosion(entity.getLocation().getX(), entity.getLocation().getY(), entity.getLocation().getZ(), 6.0F, false, false);
		    
		    entity.getWorld().createExplosion(entity.getLocation().getX(), entity.getLocation().getY(), entity.getLocation().getZ(), 6.0F, false, false);
		    entity.getWorld().createExplosion(entity.getLocation().getX(), entity.getLocation().getY(), entity.getLocation().getZ(), 6.0F, false, false);
		    entity.getWorld().createExplosion(entity.getLocation().getX(), entity.getLocation().getY(), entity.getLocation().getZ(), 6.0F, false, false);
		    
		    for (int i = 0; i < locations.size(); i++)
		    {
		      double x = RandomDouble(1.0D, -1.0D);
		      double y = RandomDouble(1.0D, -1.0D);
		      double z = RandomDouble(1.0D, -1.0D);
		      
		      FallingBlock fb = ((Location)locations.get(i)).getWorld().spawnFallingBlock((Location)locations.get(i), ((Location)locations.get(i)).getBlock().getType(), ((Location)locations.get(i)).getBlock().getData());
		      fb.setDropItem(false);
		      Vector v = new Vector(x, y, z);
		      fb.setTicksLived(20);
		      fb.setVelocity(v);
		      fb.setDropItem(false);
		      fb.setFallDistance(i);
		      locations.set(i, null);
		      uuid.add(i, fb.getUniqueId());
		      
		    }
		    

			
		}

	}
	public static double RandomDouble(double low, double high)
	  {
	    return Math.random() * (high - low) + low;
	  }
	  
	  public static Integer RandomInt(int high, int low)
	  {
	    int i = 0;
	    
	    i = new Random().nextInt(high - low) + low;
	    


	    return Integer.valueOf(i);
	  }
	
	  @EventHandler
	  public static void in(EntityChangeBlockEvent ev){
		  Entity e = ev.getEntity();
		  for (int i = 0; i < uuid.size(); i++){
			  if (uuid.get(i) == e.getUniqueId()){
				  ev.setCancelled(true);
				  ev.setCancelled(true);
				  ev.getEntity().getWorld().playEffect(ev.getEntity().getLocation(), Effect.SMOKE, 10);
				  ev.getEntity().getWorld().playEffect(ev.getEntity().getLocation(), Effect.SMOKE, 10);
				  ev.getEntity().getWorld().playEffect(ev.getEntity().getLocation(), Effect.SMOKE, 10);
				  ev.getEntity().getWorld().playEffect(ev.getEntity().getLocation(), Effect.SMOKE, 10);
				  ev.getEntity().getWorld().playEffect(ev.getEntity().getLocation(), Effect.SMOKE, 10);
				  ev.getEntity().getWorld().playEffect(ev.getEntity().getLocation(), Effect.SMOKE, 10);
				  ev.getEntity().getWorld().playEffect(ev.getEntity().getLocation(), Effect.STEP_SOUND, 10);
				  ev.getEntity().getWorld().playEffect(ev.getEntity().getLocation(), Effect.SMOKE, 10);
			  } else {
				 
			  }
		  }
	
	  }
	
}
